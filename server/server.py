import bottle
from bottle import response
from pymongo import MongoClient,DESCENDING
from json import dumps
from datetime import datetime

def enable_cors(fn):
    """
    Decorator that allows for Cross Origin Requests
    """
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

        if bottle.request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)

    return _enable_cors

#Initialize Application
app = bottle.app()

#API Route Definition
@app.route('/api/sentiment',method=["GET"])
@enable_cors
def sentiment():
    """
    API Sentiment Route

    Queries sentiments database and returns a json object with the date
    and most recent sentiment classification. 
    """
    db = MongoClient()['csc478']
    query = db.sentiments.find({},limit=1).sort('date',DESCENDING)
    result = {"date":None,"sentiment":None}
    response.headers['Content-Type'] = 'application/json'
    for item in query:
        result['date'] = item['date'].strftime("%x")
        result['sentiment'] = item['sentiment']

    return dumps(result)

app.run(host='0.0.0.0',port=3000)

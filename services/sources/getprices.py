import requests
import time
from models import Quote
from dateutil import parser

while True:
    """
    Continuously running job that pings Coindesk API every minute to get near real-time
    prices for Bitcoin. Results are stored in quotes database. 
    """
    try:
        data = requests.get('http://api.coindesk.com/v1/bpi/currentprice.json')
        json = data.json()
        quote = Quote()
        quote.ticker = json['chartName']
        quote.updated = parser.parse(json['time']['updated'])
        quote.USD = json['bpi']['USD']['rate_float']
        quote.GBP = json['bpi']['GBP']['rate_float']
        quote.EUR = json['bpi']['EUR']['rate_float']
        quote.save()
        print "Success"
        time.sleep(60)
    except:
        continue

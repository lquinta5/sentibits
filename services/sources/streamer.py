from __future__ import absolute_import, print_function

"""
Continuously running job that uses Twitter Streaming API
to insert most recent tweets in English pertaining to Bitcoin
into the tweets database.
"""
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler,Stream
import os
from pymongo import MongoClient
import json

client = MongoClient()
db = client['csc478']
tweets = db['tweets']
consumer_key = os.environ.get("CONSUMER_KEY")
consumer_secret = os.environ.get("CONSUMER_SECRET")

access_token = os.environ.get("ACCESS_TOKEN")
access_token_secret = os.environ.get("ACCESS_TOKEN_SECRET")


class StdOutListener(StreamListener):
    """
    Class definition for Steam listener that prints out to console. 
    """

    def on_data(self,data):
        tweet = json.loads(data)
        tweets.insert_one(tweet)
        print("Success")
        return True

    def on_error(self,status):
        print(status)

if __name__ == '__main__':
    
    listener = StdOutListener()
    auth = OAuthHandler(consumer_key,consumer_secret)
    auth.set_access_token(access_token,access_token_secret)

    stream = Stream(auth,listener)
    stream.filter(languages=["en"],track=["bitcoin","#bitcoin"])

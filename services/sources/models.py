import datetime
from mongoengine import Document, connect
from mongoengine import StringField, DateTimeField, DecimalField

db = 'csc478'

class Task(Document):
    title = StringField()
    text = StringField()
    creation = DateTimeField(default=datetime.datetime.now)
    meta = {'collection':'tasks'}

class Quote(Document):
    ticker = StringField(required=True)
    updated = DateTimeField(default=datetime.datetime.now)
    USD = DecimalField()
    GBP = DecimalField()
    EUR = DecimalField() 
    creation = DateTimeField(default=datetime.datetime.now)
    meta = {'collection':'quotes'}

connect(db)


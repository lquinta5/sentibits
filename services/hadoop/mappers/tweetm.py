#!/usr/bin/python

from bson.json_util import loads
import sys
import unicodedata
from datetime import datetime

"""
This mapper takes as input a JSON file output
from MongoDB where tweets pertaining to Bitcoin
for dates ranging from 05-05-2017 - 05-27-2017 were
extracted for analysis. The output of this mapper is the
key represented as the timestamp in miliseconds 
that the tweet was created as well as a value which contains 
the text of the tweet.
"""
for line in sys.stdin:
    try:
        split = loads(line.strip())
        key = datetime.fromtimestamp(int(split['timestamp_ms'])/1000).strftime("%x")
        value = unicodedata.normalize('NFD',split['text']).encode('ascii','ignore')
    except:
        continue
    print key,'\t',value

#!/usr/bin/python

import sys
import numpy as np
from textblob import TextBlob

"""
This mapper takes as input the output from the
tweetr.py script. The python textblob module
is used to perform sentiment analysis on each
of the tweets. Finally, a count of total positive, neutral,
and negative tweets as well as the total number of tweets
agregated by date is output.
"""

for line in sys.stdin:
    line = line.split('\t')
    date = line[0]
    other = line[1].strip().split("|")
    polarities = list(map(lambda x: TextBlob(x).sentiment.polarity,other))
    pos = [x for x in polarities if x > 0]
    neu = [x for x in polarities if x == 0]
    neg = [x for x in polarities if x < 0]
    print date,len(pos),len(neu),len(neg),len(polarities)

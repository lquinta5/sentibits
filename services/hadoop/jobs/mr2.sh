#!/bin/bash

#Hadoop Streaming Map Reduce job to perform sentiment analysis on mr1.sh output
time hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.6.4.jar -input /data/mr1output/part-00000 -output /data/mr2output -mapper tweetm2.py -file /home/ec2-user/sentibits/services/hadoop/mappers/tweetm2.py -reducer tweetr2.py -file /home/ec2-user/sentibits/services/hadoop/reducers/tweetr2.py

#!/bin/bash

#Hadoop Streaming Map Reduce Job to arrange tweets by daily intervals.
time hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-2.6.4.jar -input /data/tweets.json -output /data/mr1output -mapper tweetm.py -file /home/ec2-user/sentibits/services/hadoop/mappers/tweetm.py -reducer tweetr.py -file /home/ec2-user/sentibits/services/hadoop/reducers/tweetr.py

#!/usr/bin/python

"""
This reducer takes as input a date as key and tweet text as value.

The output is a single date and the text components of the tweets
for that particular date separated by the pipe delimiter.
"""

import sys

key = None
currentKey = None
tweets = ''

for line in sys.stdin:
    try:
        split = line.strip().split('\t')
        if len(split) > 1:
            key = split[0]
            value = split[1]
            if currentKey == key:
                tweets.append(value)
            else:
                if currentKey:
                    print key,'\t','|'.join(tweets)
                currentKey = key
                tweets = [value]
    except:
        continue

if currentKey:
    print key,'\t','|'.join(tweets)

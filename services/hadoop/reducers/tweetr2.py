#!/usr/bin/python

import sys

"""
This reducer takes the input from the tweetm2.py
mapper and outputs to HDFS
"""

for line in sys.stdin:
    print line.strip()

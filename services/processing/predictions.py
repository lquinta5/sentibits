#!/usr/bin/python

import sys
from textblob import TextBlob
from bson.json_util import loads
import unicodedata
import datetime
from pymongo import MongoClient
from hmmlearn import hmm
from sklearn.externals import joblib
import numpy as np
import pandas as pd
import os

db = MongoClient()['csc478']

def unix_time_millis(dt):
    """
    Convert datetime object to milliseconds
    """
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds()

def get_tweets(query,today):
    """
    Query tweets database for tweets posted on current date.
    Then sentiment analysis on these tweets is performed 
    and the date, count of positive,negative and neutral tweets
    is returned. 
    """
    tweets = []
    date = today
    for document in query:
        txt = TextBlob(unicodedata.normalize('NFD',document['text']).encode('ascii','ignore'))
        tweets.append(txt)
        
    pos = [x for x in tweets if x.sentiment.polarity > 0]
    neg = [x for x in tweets if x.sentiment.polarity < 0]
    neu = [x for x in tweets if x.sentiment.polarity == 0]
    
    return date,len(pos),len(neg),len(neu)

def insert_or_update(today,next_prediction):
    """
    Insert or update latest sentiment record in sentiments database
    """
    q = db.sentiments.find({"date":{"$eq":today,"$exists":True}}).count()
    if q == 1:
        db.sentiments.replace_one({"date":today},{"date":today,"sentiment":next_prediction[0]})
        print "Replaced Doc"
    else:
        db.sentiments.insert_one({"date":today,"sentiment":next_prediction[0]})
        print "Inserted Doc"

def main():
    """
    Main method. Runs every hour to account for latest tweets.
    """
    today = datetime.datetime.combine(datetime.date.today(),datetime.time.min)
    tomorrow = today + datetime.timedelta(days=1)    
    query = db.tweets.find({"timestamp_ms": {
        "$gte": str(unix_time_millis(today)),
        "$lt": str(unix_time_millis(tomorrow))
    }},projection=["timestamp_ms","text"])
    date,pos,neg,neu = get_tweets(query,today)
    hmmModel = joblib.load('/home/ec2-user/sentibits/models/model/hmm.pkl')
    next_prediction = hmmModel.predict([pos,neg])
    insert_or_update(today,next_prediction)

if __name__ == "__main__":
    main()

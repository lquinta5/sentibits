var webpack = require('webpack');

var config = {
    context: __dirname + '/src',
    entry: {
	app: ['./app.js','angular','angular-route','highcharts-ng','lodash']
    },
    output: {
	path: __dirname + '/dist',
	filename: 'bundle.js'
    },
    devServer: {
	host: "0.0.0.0",
	contentBase: __dirname + '/src',
	compress: true,
	disableHostCheck: true
    }
}

module.exports = config;

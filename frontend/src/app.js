require('angular');
require('highcharts-ng');
var lodash = require('lodash');
var moment = require('moment');

/**
 * Application Initialization
 */
var app = angular.module('myApp',['ngRoute','highcharts-ng']);

/**
 * Application Route Config
 */

app.config(['$routeProvider',function($routeProvider){
    $routeProvider.
	when('/',{
	    templateUrl: '/views/home.html',
	    controller: 'homeController'
	})
	.otherwise('/');
}]);

/**
 * Application Constant Definitions
 */
app.constant('lodash',lodash);
app.constant('moment',moment);

app.controller('homeController',['$scope','$http','$q','moment',function($scope,$http,$q,moment){
    /**
     * ACCESSIBLE MEMBERS
     */
    
    $scope.message = "Welcome to Sentibits";
    $scope.dates = [];
    $scope.prices = [];
    $scope.prediction_date = null
    $scope.sentiment_prediction = null

    /**
     * 1 Month Date Range Variable Definitions
     */
    var start = moment().subtract(1,'M').format('YYYY-MM-DD');
    var end = moment().format("YYYY-MM-DD");
    
    /**
     * HTTP Request Config for Coindesk API
     */
    var price_config = {
        method: 'GET',
        url: 'http://api.coindesk.com/v1/bpi/historical/close.json',
        params: {
            "start_date": start,
            "end_date": end
        }
    };

    /**
     * AJAX HTTP Request for Last Month's Prices
     */
    $http(price_config)
	.then(function(response){
	    prices = response.data.bpi;
	    _.forOwn(prices,function(value,key){
		$scope.dates.push(key);
		$scope.prices.push(value);
	    });
	},function(error){
	    console.log(error);
	});

    /**
     * HTTP REquest Config for Sentibits Backend
     */

    var sentiment_config = {
        method: 'GET',
        url: 'http://54.213.155.92:3000/api/sentiment',
        headers: {
            "Accept": "application/json"
        }
    };

    /**
     * AJAX HTTP Request for Sentiment Analysis Prediction
     */
    $http(sentiment_config)
        .then(function(response){
        
            $scope.prediction_date = response.data['date'];
            if (response.data['sentiment'] == 0) {
                $scope.sentiment_prediction = "Positive";
            } else {
                $scope.sentiment_prediction = "Negative";
            }          
        },function(error){
            console.log(error)
        });

    /**
     * Monthly Price Chart Definition
     */

    $scope.chartConfig = {
	chart: {
	    backgroundColor: '#000000'
	},

        title: {
            text: 'Bitcoin Price Index',
	    style: {
		color: '#FFFFFF'
	    }
        },

        subtitle: {
            text: 'Source: coindesk.com'
        },

        yAxis: {
            title: {
                text: 'Price',
		style: {
		    color: "#FFFFFF"
		}
            },
	    labels: {
		style: {
		    color: "#FFFFFF"
	    
		}
	    }
        },
        xAxis: {
            categories: $scope.dates,
	    labels: {
		style: {
		    color: "#FFFFFF"
		}
	    }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
	    itemStyle: {
		color: "#FFFFFF"
	    }
        },

        plotOptions: {
            series: {
                pointStart: 2010
            }
        },

        series: [{
            name: 'Bitcoin',
            data: $scope.prices,
            color: '#FFD700'
        }]
    }
}]);
